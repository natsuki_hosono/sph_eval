#pragma once
namespace STD{
	void CalcPressure(PS::ParticleSystem<STD::RealPtcl>& sph_system){
		#pragma omp parallel for
		for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].pres = sph_system[i].EoS->Pressure(sph_system[i].dens, 0.0);
			sph_system[i].snds = sph_system[i].EoS->SoundSpeed(sph_system[i].dens, 0.0);
		}
	}
	class MemoryTransfer{
		public:
		void operator () (const EPI::Hydro* const ep_i, const PS::S32 Nip, const EPJ::Hydro* const ep_j, const PS::S32 Njp, RESULT::Hydro* const hydro){
			#ifdef __AVX__
				#warning MEM TRANSFER: AVX512
				for(PS::S32 i = 0 ; i < Nip ; i += 8){
					asm("# Hydro Start");
					asm("# Set 0");
					__m512d iax = _mm512_setzero_pd();
					__m512d iay = _mm512_setzero_pd();
					__m512d iaz = _mm512_setzero_pd();
					
					//__builtin_prefetch(&ep_j[0].pos.x, 0, 3);
					//__builtin_prefetch(&ep_j[0].pos.y, 0, 3);
					//__builtin_prefetch(&ep_j[0].pos.z, 0, 3);

					asm("# gather i-ptcls");
					const __mmask8 mask = _mm512_cmp_epu64_mask((__m512i{0, 1, 2, 3, 4, 5, 6, 7}), _mm512_set1_epi64(Nip - i), _MM_CMPINT_LT);
					const __m512i gather_idx = _mm512_mullox_epi64(__m512i{0, 1, 2, 3, 4, 5, 6, 7}, _mm512_set1_epi64(sizeof(EPI::Hydro)));
					const __m512d irx = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].pos.x, 1);
					const __m512d iry = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].pos.y, 1);
					const __m512d irz = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].pos.z, 1);
					const __m512d idens = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].dens, 1);
					const __m512d ipres = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].pres, 1);
					const __m512d ismth = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].smth, 1);
					asm("# j-loop start");
					for(PS::S32 j = 0; j < Njp ; ++ j){
						const __m512d jrx = _mm512_set1_pd(ep_j[j].pos.x);
						const __m512d jry = _mm512_set1_pd(ep_j[j].pos.y);
						const __m512d jrz = _mm512_set1_pd(ep_j[j].pos.z);
						const __m512d jmass = _mm512_set1_pd(ep_j[j].mass);
						const __m512d jpres = _mm512_set1_pd(ep_j[j].pres);
						const __m512d jdens = _mm512_set1_pd(ep_j[j].dens);
					}
					asm("# j-loop end");
					const __m512i scatter_idx = _mm512_mullox_epi64(__m512i{0, 1, 2, 3, 4, 5, 6, 7}, _mm512_set1_epi64(sizeof(RESULT::Hydro)));
					_mm512_mask_i64scatter_pd(&hydro[i].acc.x, mask, scatter_idx, iax, 1);
					_mm512_mask_i64scatter_pd(&hydro[i].acc.y, mask, scatter_idx, iay, 1);
					_mm512_mask_i64scatter_pd(&hydro[i].acc.z, mask, scatter_idx, iaz, 1);
					asm("# Hydro end");
				}
			#else
				#warning MEM TRANSFER: NO SIMD
				asm volatile("# copy kernel start");
				for(PS::S32 i = 0; i < Nip ; ++ i){
					asm volatile("# i-loop start");
					asm volatile("# zero clear acc");
					PS::F64vec acc = 0.0;
					asm volatile("# copy ith");
					const EPI::Hydro ith = ep_i[i];
					asm volatile("# j-copy kernel ith");
					for(PS::S32 j = 0; j < Njp ; ++ j){
						asm volatile("# j-loop start: copy jth");
						const EPJ::Hydro jth = ep_j[j];
						asm volatile("# j-loop fin");
					}
					asm volatile("# writeback acc");
					hydro[i].acc = acc;
					asm volatile("# i-loop fin");
				}
				asm volatile("# copy kernel fin");
			#endif
		}
	};
	class CalcHydroForce{
		const kernel_t kernel;
		public:
		void operator () (const EPI::Hydro* const ep_i, const PS::S32 Nip, const EPJ::Hydro* const ep_j, const PS::S32 Njp, RESULT::Hydro* const hydro){
			asm("# LLVM-MCA-BEGIN");
			#ifdef __AVX__
				#if 1
				#warning force AVX512F on
				for(PS::S32 i = 0 ; i < Nip ; i += 8){
					asm("# Hydro Start");
					asm("# Set 0");
					__m512d iax = _mm512_setzero_pd();
					__m512d iay = _mm512_setzero_pd();
					__m512d iaz = _mm512_setzero_pd();
					
					//__builtin_prefetch(&ep_j[0].pos.x, 0, 3);
					//__builtin_prefetch(&ep_j[0].pos.y, 0, 3);
					//__builtin_prefetch(&ep_j[0].pos.z, 0, 3);

					asm("# gather i-ptcls");
					const __mmask8 mask = _mm512_cmp_epu64_mask((__m512i{0, 1, 2, 3, 4, 5, 6, 7}), _mm512_set1_epi64(Nip - i), _MM_CMPINT_LT);
					const __m512i gather_idx = _mm512_mullox_epi64(__m512i{0, 1, 2, 3, 4, 5, 6, 7}, _mm512_set1_epi64(sizeof(EPI::Hydro)));
					const __m512d irx = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].pos.x, 1);
					const __m512d iry = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].pos.y, 1);
					const __m512d irz = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].pos.z, 1);
					const __m512d idens = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].dens, 1);
					const __m512d ipres = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].pres, 1);
					const __m512d ismth = _mm512_mask_i64gather_pd(_mm512_setzero_pd(), mask, gather_idx, &ep_i[i].smth, 1);
					const __m512d ith_p_over_rho2 = _mm512_div_pd(ipres, _mm512_mul_pd(idens, idens));
					asm("# j-loop start");
					for(PS::S32 j = 0; j < Njp ; ++ j){
						/*
						__builtin_prefetch(&ep_j[j + 1].pos.x, 0, 3);
						__builtin_prefetch(&ep_j[j + 1].pos.y, 0, 3);
						__builtin_prefetch(&ep_j[j + 1].pos.z, 0, 3);
						__builtin_prefetch(&ep_j[j + 1].mass, 0, 3);
						__builtin_prefetch(&ep_j[j + 1].pres, 0, 3);
						__builtin_prefetch(&ep_j[j + 1].dens, 0, 3);
						*/
						const __m512d jrx = _mm512_set1_pd(ep_j[j].pos.x);
						const __m512d jry = _mm512_set1_pd(ep_j[j].pos.y);
						const __m512d jrz = _mm512_set1_pd(ep_j[j].pos.z);
						const __m512d drx = _mm512_sub_pd(jrx, irx);
						const __m512d dry = _mm512_sub_pd(jry, iry);
						const __m512d drz = _mm512_sub_pd(jrz, irz);
						const __m512d smth = _mm512_mul_pd(_mm512_set1_pd(0.5), _mm512_add_pd(ismth, _mm512_set1_pd(ep_j[j].smth)));
						const __m512d dr2 = _mm512_fmadd_pd(drx, drx, _mm512_fmadd_pd(dry, dry, _mm512_fmadd_pd(drz, drz, 0.0001 * smth * smth)));
						#if 1
						const __m512d dr  = _mm512_sqrt_pd(dr2);
						const __m512d drinv = _mm512_div_pd(_mm512_set1_pd(1.0), dr);
						#else
						const __m512d drinv = _mm512_rsqrt14_pd(dr2);
						const __m512d dr  = dr2 * drinv;
						#endif
						const __m512d jmass = _mm512_set1_pd(ep_j[j].mass);
						const __m512d jpres = _mm512_set1_pd(ep_j[j].pres);
						const __m512d jdens = _mm512_set1_pd(ep_j[j].dens);
						const __m512d abs_gradW = kernel.gradW(dr, smth);
						const __m512d acc_drinv = jmass * (ith_p_over_rho2 + jpres / (jdens * jdens)) * abs_gradW * drinv;
						asm("#  accumulate");
						iax = _mm512_fmadd_pd(acc_drinv, drx, iax);
						iay = _mm512_fmadd_pd(acc_drinv, dry, iay);
						iaz = _mm512_fmadd_pd(acc_drinv, drz, iaz);
					}
					asm("# j-loop end");
					const __m512i scatter_idx = _mm512_mullox_epi64(__m512i{0, 1, 2, 3, 4, 5, 6, 7}, _mm512_set1_epi64(sizeof(RESULT::Hydro)));
					_mm512_mask_i64scatter_pd(&hydro[i].acc.x, mask, scatter_idx, iax, 1);
					_mm512_mask_i64scatter_pd(&hydro[i].acc.y, mask, scatter_idx, iay, 1);
					_mm512_mask_i64scatter_pd(&hydro[i].acc.z, mask, scatter_idx, iaz, 1);
					asm("# Hydro end");
				}
				#else
				#warning force AVX2 on
				for(PS::S32 i = 0 ; i < Nip ; i += 4){
					__m256d iax = _mm256_setzero_pd();
					__m256d iay = _mm256_setzero_pd();
					__m256d iaz = _mm256_setzero_pd();

					const EPI::Hydro ith[4]{ep_i[i], ep_i[std::min(i + 1, Nip - 1)], ep_i[std::min(i + 2, Nip - 1)], ep_i[std::min(i + 3, Nip - 1)]};

					const __m256d irx{ith[0].pos.x, ith[1].pos.x, ith[2].pos.x, ith[3].pos.x};
					const __m256d iry{ith[0].pos.y, ith[1].pos.y, ith[2].pos.y, ith[3].pos.y};
					const __m256d irz{ith[0].pos.z, ith[1].pos.z, ith[2].pos.z, ith[3].pos.z};

					const __m256d idens{ith[0].dens, ith[1].dens, ith[2].dens, ith[3].dens};
					const __m256d ipres{ith[0].pres, ith[1].pres, ith[2].pres, ith[3].pres};
					const __m256d ismth{ith[0].smth, ith[1].smth, ith[2].smth, ith[3].smth};
					const __m256d ith_p_over_rho2 = _mm256_div_pd(ipres, _mm256_mul_pd(idens, idens));
					
					for(PS::S32 j = 0; j < Njp ; ++ j){
						asm("# Hydro Start");
						const __m256d drx = _mm256_sub_pd(irx, _mm256_broadcast_sd(&ep_j[j].pos.x));
						const __m256d dry = _mm256_sub_pd(iry, _mm256_broadcast_sd(&ep_j[j].pos.y));
						const __m256d drz = _mm256_sub_pd(irz, _mm256_broadcast_sd(&ep_j[j].pos.z));
						const __m256d smth = 0.5 * _mm256_add_pd(ismth, _mm256_broadcast_sd(&ep_j[j].smth));
						const __m256d dr2 = _mm256_fmadd_pd(drx, drx, _mm256_fmadd_pd(dry, dry, _mm256_fmadd_pd(drz, drz, 0.0001 * smth * smth)));
						const __m256d dr  = _mm256_sqrt_pd(dr2);

						const __m256d drinv = 1.0 / dr;
						const __m256d jmass = _mm256_broadcast_sd(&ep_j[j].mass);
						const __m256d jpres = _mm256_broadcast_sd(&ep_j[j].pres);
						const __m256d jdens = _mm256_broadcast_sd(&ep_j[j].dens);
						const __m256d abs_gradW = kernel.gradW(dr, smth);
						const __m256d acc = jmass * (ith_p_over_rho2 + jpres / (jdens * jdens)) * abs_gradW;
						iax -= acc * drx * drinv;
						iay -= acc * dry * drinv;
						iaz -= acc * drz * drinv;
						asm("# Hydro End");
					}
					for(int simd = 0 ; simd < 4 ; ++ simd){
						const int i_ = std::min(i + simd, Nip - 1);
						hydro[i_].acc.x = iax[simd];
						hydro[i_].acc.y = iay[simd];
						hydro[i_].acc.z = iaz[simd];
					}
				}
				#endif
			#elif __ARM_FEATURE_SVE
				#warning force ARM SVE on
				const svuint64_t gather_offset = svindex_u64(0, sizeof(EPI::Hydro));
				const svuint64_t scatter_offset = svindex_u64(0, sizeof(RESULT::Hydro));
				for(PS::S32 i = 0 ; i < Nip ; i += svcntd()){
					const svbool_t pg = svwhilelt_b64(i, Nip);
					svfloat64_t iax = svdup_f64_z(pg, 0.0);
					svfloat64_t iay = svdup_f64_z(pg, 0.0);
					svfloat64_t iaz = svdup_f64_z(pg, 0.0);

					const svfloat64_t irx = svld1_gather_offset(pg, &ep_i[i].pos.x, gather_offset);
					const svfloat64_t iry = svld1_gather_offset(pg, &ep_i[i].pos.y, gather_offset);
					const svfloat64_t irz = svld1_gather_offset(pg, &ep_i[i].pos.z, gather_offset);

					const svfloat64_t idens = svld1_gather_offset(pg, &ep_i[i].dens, gather_offset);
					const svfloat64_t ipres = svld1_gather_offset(pg, &ep_i[i].pres, gather_offset);
					const svfloat64_t ismth = svld1_gather_offset(pg, &ep_i[i].smth, gather_offset);
					const svfloat64_t ith_p_over_rho2 = svdiv_x(pg, ipres, svmul_x(pg, idens, idens));
					for(PS::S32 j = 0; j < Njp ; ++ j){
						const svfloat64_t drx = svsubr_z(pg, irx, ep_j[j].pos.x);
						const svfloat64_t dry = svsubr_z(pg, iry, ep_j[j].pos.y);
						const svfloat64_t drz = svsubr_z(pg, irz, ep_j[j].pos.z);
						const svfloat64_t smth = svmul_z(pg, svadd_z(pg, ismth, ep_j[j].smth), 0.5);
						const svfloat64_t dr2 = svmad_z(pg, drx, drx, svmad_z(pg, dry, dry, svmad_z(pg, drz, drz, svmul_z(pg, svmul_z(pg, smth, smth), 0.0001))));
						const svfloat64_t dr = svsqrt_z(pg, dr2);
						const svfloat64_t drinv = svdivr_z(pg, dr, 1.0);
						const svfloat64_t jmass = svdup_f64_z(pg, ep_j[j].mass);
						const svfloat64_t jpres = svdup_f64_z(pg, ep_j[j].pres);
						const svfloat64_t jdens = svdup_f64_z(pg, ep_j[j].dens);
						const svfloat64_t abs_gradW = kernel.gradW(pg, dr, smth);
						//
						svfloat64_t acc = svmul_z(pg, jdens, jdens);//jdens * jdens
						acc = svdiv_z(pg, jpres, acc); //jpres / (jdens * jdens)
						acc = svadd_z(pg, ith_p_over_rho2, acc); // ith_p_over_rho2 + jpres / (jdens * jdens)
						acc = svmul_z(pg, jmass, acc); // jmass * (ith_p_over_rho2 + jpres / (jdens * jdens))
						acc = svmul_z(pg, abs_gradW, acc); // jmass * (ith_p_over_rho2 + jpres / (jdens * jdens)) * abs_gradW
						acc = svmul_z(pg, drinv, acc); //jmass * (ith_p_over_rho2 + jpres / (jdens * jdens)) * abs_gradW * drinv
						iax = svmad_z(pg, acc, drx, iax);
						iay = svmad_z(pg, acc, dry, iay);
						iaz = svmad_z(pg, acc, drz, iaz);
					}
					svst1_scatter_offset(pg, &hydro[i].acc.x, scatter_offset, iax);
					svst1_scatter_offset(pg, &hydro[i].acc.y, scatter_offset, iay);
					svst1_scatter_offset(pg, &hydro[i].acc.z, scatter_offset, iaz);
				}
			#else
				#warning standard impl.
				#ifdef FJCOLL
					#warning FJCOLL on
					start_collection("Hydro");
				#endif
				for(PS::S32 i = 0; i < Nip ; ++ i){
					PS::F64vec acc = 0.0;
					const EPI::Hydro ith = ep_i[i];
					const double ith_p_over_rho2 = ith.pres / (ith.dens * ith.dens);
					for(PS::S32 j = 0; j < Njp ; ++ j){
						const EPJ::Hydro jth = ep_j[j];
						const PS::F64vec dr = ith.pos - jth.pos;
						//const PS::F64vec dv = ith.vel - jth.vel;
						//const PS::F64vec gradW = 0.5 * (kernel.gradW(dr, ith.smth) + kernel.gradW(dr, jth.smth));
						const PS::F64vec gradW = kernel.gradW(dr, (ith.smth + jth.smth) * 0.5);
						//pressure gradient
						acc -= jth.mass * (ith_p_over_rho2 + jth.pres / (jth.dens * jth.dens)) * gradW;
					}
					hydro[i].acc = acc;
				}
				#ifdef FJCOLL
					#warning FJCOLL on
					stop_collection("Hydro");
				#endif
			#endif
			asm("# LLVM-MCA-END");
		}
	};
}


