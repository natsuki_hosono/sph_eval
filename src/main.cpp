#include <particle_simulator.hpp>
#include <sys/time.h>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>

#ifdef __AVX__
#warning Intel AVX ACTIVATED
#include <x86intrin.h>
#endif

#ifdef __ARM_FEATURE_SVE
#warning ARM SVE ACTIVATED
#include <arm_sve.h>
#endif

#include "param.h"
#include "kernel.h"
#include "EoS.h"
#include "class.h"
//=============
#include "init/cube.h"
//=============
#include "force.h"
#include "io.h"

int main(int argc, char* argv[]){
	const std::size_t loop = 2;
	//Test();
	std::cerr << "input Ngrp" << std::endl;
	std::cin >> PARAM::Ngroup;
	//PARAM::Ngroup = 32;
	std::cerr << "set Ngrp to" << PARAM::Ngroup << std::endl;

	namespace PTCL = STD;
	typedef Dam<PTCL::RealPtcl> PROBLEM;
	//////////////////
	//Create vars.
	//////////////////
	PS::Initialize(argc, argv);
	PS::ParticleSystem<PTCL::RealPtcl> sph_system;
	sph_system.initialize();
	PS::DomainInfo dinfo;
	dinfo.initialize(0.3);
	system_t sysinfo;
	sph_system.setAverageTargetNumberOfSampleParticlePerProcess(200);

	//////////////////
	//Setup Initial
	//////////////////
	PROBLEM::setupIC(sph_system, sysinfo, dinfo);
	#pragma omp parallel for
	for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
		sph_system[i].initialize();
	}
	PTCL::CalcPressure(sph_system);
	//Display info
	if(PS::Comm::getRank() == 0){
		std::cout << "# of thds = " << PS::Comm::getNumberOfThread() << std::endl;
	}

	//Domain info
	dinfo.decomposeDomainAll(sph_system);
	sph_system.exchangeParticle(dinfo);
	//plant tree
	PS::TreeForForceShort<PTCL::RESULT::Hydro, PTCL::EPI::Hydro, PTCL::EPJ::Hydro>::Symmetry hydr_tree;

	hydr_tree.initialize(sph_system.getNumberOfParticleLocal(), 0.5, PARAM::Nleaf, PARAM::Ngroup);
	#if 1
	for(int i = 0 ; i < loop ; ++ i){
		std::cout << "TRANSFER TIME: " << i << std::endl;
		hydr_tree.clearTimeProfile();
		hydr_tree.calcForceAllAndWriteBack(PTCL::MemoryTransfer(), sph_system, dinfo, true, PS::MAKE_LIST_FOR_REUSE);
		hydr_tree.getTimeProfile().dump();
	}
	#endif
	for(int i = 0 ; i < loop ; ++ i){
		std::cout << "HYDRO TIME: MAKE LIST " << i << std::endl;
		hydr_tree.clearTimeProfile();
		hydr_tree.calcForceAllAndWriteBack(PTCL::CalcHydroForce(), sph_system, dinfo, true, PS::MAKE_LIST_FOR_REUSE);
		hydr_tree.getTimeProfile().dump();

		std::cout << "HYDRO TIME: REUSE LIST " << i << std::endl;
		hydr_tree.clearTimeProfile();
		hydr_tree.calcForceAllAndWriteBack(PTCL::CalcHydroForce(), sph_system, dinfo, true, PS::REUSE_LIST);
		hydr_tree.getTimeProfile().dump();
	}
	
	OutputFileWithTimeInterval(sph_system, sysinfo, PROBLEM::END_TIME);

	PS::Finalize();
	return 0;
}

