#pragma once
template <class ThisPtcl> void OutputFileWithTimeInterval(PS::ParticleSystem<ThisPtcl>& sph_system, const system_t& sysinfo, const PS::F64 end_time){
	static PS::F64 time = sysinfo.time;
	static PS::S64 step = sysinfo.step;
	char filename[256];
	//Ascii
	FileHeader header;
	header.time = sysinfo.time;
	header.Nbody = sph_system.getNumberOfParticleLocal();
	sprintf(filename, "result/%05lld", step);
	sph_system.writeParticleAscii(filename, "%s_%05d_%05d.dat", header);
	if(PS::Comm::getRank() == 0){
		std::cerr << "//================================" << std::endl;
		std::cerr << "output " << filename << "." << std::endl;
		std::cerr << "//================================" << std::endl;
	}
	return;
}

