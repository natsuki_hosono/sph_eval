#pragma once

struct KernelFunction{
	static constexpr double pi = M_PI;//atan(1.0) * 4.0;
	virtual PS::F64 W(const PS::F64vec, const PS::F64) const = 0;
	virtual PS::F64vec gradW(const PS::F64vec, const PS::F64) const = 0;
	PS::F64 plus(const PS::F64 arg) const{
		return (arg > 0) ? arg : 0;
	}
	PS::F64 pow8(const PS::F64 arg) const{
		const PS::F64 arg2 = arg * arg;
		const PS::F64 arg4 = arg2 * arg2;
		return arg4 * arg4;
	}
	PS::F64 pow7(const PS::F64 arg) const{
		const PS::F64 arg3 = arg * arg * arg;
		return arg3 * arg3 * arg;
	}
	PS::F64 pow4(const PS::F64 arg) const{
		const PS::F64 arg2 = arg * arg;
		return arg2 * arg2;
	}
	PS::F64 pow3(const PS::F64 arg) const{
		const PS::F64 arg2 = arg * arg;
		return arg * arg2;
	}
	static PS::F64 supportRadius();
	#ifdef __ARM_FEATURE_SVE
	svfloat64_t plus(const svbool_t pg, const svfloat64_t arg) const{
		return svmax_z(pg, arg, 0.0);
	}
	svfloat64_t pow3(const svbool_t pg, const svfloat64_t arg) const{
		return svmul_z(pg, arg, svmul_z(pg, arg, arg));
	}
	svfloat64_t pow4(const svbool_t pg, const svfloat64_t arg) const{
		const svfloat64_t arg2 = svmul_z(pg, arg, arg);
		return svmul_z(pg, arg2, arg2);
	}
	#endif
	#ifdef __AVX2__
	#warning AVX2 on
	__m256d plus(const __m256d arg) const{
		return _mm256_max_pd(arg, _mm256_setzero_pd());
	}
	__m256d pow3(const __m256d arg) const{
		return arg * arg * arg;
	}
	__m256d pow4(const __m256d arg) const{
		const __m256d arg2 = arg * arg;
		return arg2 * arg2;
	}
	#endif
	#ifdef __AVX512F__
	#warning AVX512F on
	__m512d plus(const __m512d arg) const{
		return _mm512_max_pd(arg, _mm512_setzero_pd());
	}
	__m512d pow3(const __m512d arg) const{
		return arg * arg * arg;
	}
	__m512d pow4(const __m512d arg) const{
		const __m512d arg2 = arg * arg;
		return arg2 * arg2;
	}
	__m512d pow7(const __m512d arg) const{
		const __m512d arg2 = arg * arg;
		const __m512d arg3 = arg2 * arg;
		return arg3 * arg3 * arg;
	}
	__m512d pow8(const __m512d arg) const{
		const __m512d arg2 = arg * arg;
		const __m512d arg4 = arg2 * arg2;
		return arg4 * arg4;
	}
	#endif
};


struct WendlandC2 : public KernelFunction{
	WendlandC2(){
	}
	//W
	PS::F64 W(const PS::F64vec dr, const PS::F64 h) const{
		const PS::F64 H = supportRadius() * h;
		const PS::F64 s = sqrt(dr * dr) / H;
		PS::F64 r_value;
		r_value = (1.0 + s * 4.0) * pow4(plus(1.0 - s));
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
		r_value *= 7. / (H * H * pi);
		#else
		r_value *= 21./2. / (H * H * H * pi);
		#endif
		return r_value;
	}
	//gradW
	PS::F64vec gradW(const PS::F64vec dr, const PS::F64 h) const{
		const PS::F64 H = supportRadius() * h;
		const PS::F64 dist = sqrt(dr * dr + 0.0001 * h * h);
		const PS::F64 s = dist / H;
		PS::F64 r_value;
		r_value = 4.0 * pow4(plus(1.0 - s)) - 4.0 * pow3(plus(1.0 - s)) * (1.0 + s * 4.0);
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
		r_value *= 7. / (H * H * pi);
		#else
		r_value *= 21./2. / (H * H * H * pi);
		#endif
		return dr * r_value / (dist * H);
	}
	#ifdef __ARM_FEATURE_SVE
	svfloat64_t gradW(const svbool_t pg, const svfloat64_t dr, const svfloat64_t h) const{
		const svfloat64_t H = svmul_z(pg, h, supportRadius());
		const svfloat64_t s = svdiv_z(pg, dr, H);
		const svfloat64_t plus_one_minus_s = plus(pg, svsubr_z(pg, s, 1.0));
		svfloat64_t r_value = svsub_z(pg, svmul_z(pg, pow4(pg, plus_one_minus_s), 4.0), svmul_z(pg, svmul_z(pg, pow3(pg, plus_one_minus_s), svadd_z(pg, svmul_z(pg, s, 4.0), 1.0)), 4.0));
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
			#error TBW
		#else
		r_value = svdiv_z(pg, svmul_z(pg, r_value, 10.5 / pi), pow3(pg, H));
		#endif
		return svdiv_z(pg, r_value, H);
	}
	#endif
	#ifdef __AVX__
	template <typename AVXtype> AVXtype gradW(const AVXtype dr, const AVXtype h) const{
		const AVXtype H = supportRadius() * h;
		const AVXtype s = dr / H;
		AVXtype r_value = 4.0 * pow4(plus(1.0 - s)) - 4.0 * pow3(plus(1.0 - s)) * (1.0 + s * 4.0);
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
		r_value *= 7. / (H * H * pi);
		#else
		r_value *= 21./2. / (H * H * H * pi);
		#endif
		return r_value / H;
	}
	#endif
	static PS::F64 supportRadius(){
		return 2.0;
	}
};

//Wendland C6
struct WendlandC6 : public KernelFunction{
	WendlandC6(){
	}
	//W
	PS::F64 W(const PS::F64vec dr, const PS::F64 h) const{
		const PS::F64 H = supportRadius() * h;
		const PS::F64 s = sqrt(dr * dr) / H;
		PS::F64 r_value;
		r_value = (1.0 + s * (8.0 + s * (25.0 + s * (32.0)))) * pow8(plus(1.0 - s));
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
		r_value *= (78./7.) / (H * H * pi);
		#else
		r_value *= (1365./64.) / (H * H * H * pi);
		#endif
		return r_value;
	}
	//gradW
	PS::F64vec gradW(const PS::F64vec dr, const PS::F64 h) const{
		const PS::F64 H = supportRadius() * h;
		const PS::F64 s = sqrt(dr * dr) / H;
		PS::F64 r_value;
		r_value = pow7(plus(1.0 - s)) * (plus(1.0 - s) * (8.0 + s * (50.0 + s * (96.0))) - 8.0 * (1.0 + s * (8.0 + s * (25.0 + s * (32.0)))));
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
		r_value *= (78./7.) / (H * H * pi);
		#else
		r_value *= (1365./64.) / (H * H * H * pi);
		#endif
		return dr * r_value / (sqrt(dr * dr) * H  + 1.0e-6 * h);
	}
	static PS::F64 supportRadius(){
		return 3.1;
	}
};

typedef WendlandC2 kernel_t;

