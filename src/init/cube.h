#pragma once

template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static constexpr double END_TIME = 5.0;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x = 1.0;
		const PS::F64 box_y = 1.0;
		const PS::F64 box_z = 1.0;
		#if 1
		const PS::F64 dx = box_y / 100.0;
		#else
		int N;
		std::cerr << "input N:" << std::endl;
		std::cin >> N;
		const PS::F64 dx = box_y / N;
		#endif
		PS::S32 cnt = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				for (PS::F64 z = 0; z < box_z; z += dx) {
					++cnt;
					Ptcl ith;
					ith.type = HYDRO;
					ith.tag = 0;
					ith.pos.x = x;
					ith.pos.y = y;
					ith.pos.z = z;
					ith.dens = EoS::Water.ReferenceDensity() * (1.0 + 0.1 * (double)rand() / (double)RAND_MAX);
					ith.mass = ith.dens * dx * dx * dx;
					ith.id = cnt;
					ith.EoS = &EoS::Water;
					ith.visc = &Viscosity::Water;
					ptcl.push_back(ith);
				}
			}
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XYZ);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0 ,0.0), PS::F64vec(box_x, box_y, box_z));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				ptcl[i].vel.x = 100.0 * (1.0 - 2.0 * (double)rand() / (double)RAND_MAX);
				ptcl[i].vel.y = 100.0 * (1.0 - 2.0 * (double)rand() / (double)RAND_MAX);
				ptcl[i].vel.z = 100.0 * (1.0 - 2.0 * (double)rand() / (double)RAND_MAX);
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
	}
};

