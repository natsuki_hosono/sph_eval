#pragma once

class Timer{
	timeval from, to;
	public:
	Timer(){
		start();
	}
	void start(){
		gettimeofday(&from, NULL);
	}
	void stop(){
		gettimeofday(&to, NULL);
	}
	double getWallclockTime() const{
		return to.tv_sec - from.tv_sec + (double)(to.tv_usec - from.tv_usec) / 1000000;
	}
};

enum TYPE{
	HYDRO,
	FREEZE,
};

struct system_t{
	PS::F64 dt, time;
	PS::S64 step;
	system_t() : step(0), time(0.0), dt(0.0){
	}
};

class FileHeader{
public:
	int Nbody;
	double time;
	int readAscii(FILE* fp){
		fscanf(fp, "%16.16lf\n", &time);
		fscanf(fp, "%d\n", &Nbody);
		return Nbody;
	}
	void writeAscii(FILE* fp) const{
		fprintf(fp, "%e\n", time);
		fprintf(fp, "%d\n", Nbody);
	}
};

namespace STD{
	namespace RESULT{
		//Hydro force
		class Hydro{
			public:
			PS::F64vec acc;
			PS::F64 dt;
			void clear(){
				acc = 0.0;
				dt = 1.0e+30;
			}
		};
	}

	class RealPtcl{
		public:
		PS::F64 mass;
		PS::F64vec pos, vel, acc;
		PS::F64 dens;//DENSity
		PS::F64 pres;//PRESsure
		PS::F64 smth;//SMooTHing length
		PS::F64 snds; //SouND Speed
		PS::F64 div_v;
		PS::F64 dt;
		PS::S64 id, tag;

		const EoS::EoS_t<PS::F64>* EoS;
		const Viscosity::Viscosity_t<PS::F64>* visc;

		TYPE type;
		//Constructor
		RealPtcl(){
			type = HYDRO;
		}
		//Copy functions
		void copyFromForce(const RESULT::Hydro& force){
			this->acc     = force.acc;
			this->dt      = force.dt;
		}
		//Give necessary values to FDPS
		PS::F64 getCharge() const{
			return this->mass;
		}
		PS::F64vec getPos() const{
			return this->pos;
		}
		PS::F64 getRSearch() const{
			return kernel_t::supportRadius() * this->smth;
		}
		void setPos(const PS::F64vec& pos){
			this->pos = pos;
		}
		void writeAscii(FILE* fp) const{
			#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
			fprintf(fp, "%ld\t%ld\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\n",  id,  tag,  mass,  pos.x,  pos.y,  0.0  ,  vel.x,  vel.y,  0.0  ,  dens,  0.0,  pres);
			#else
			//fprintf(fp, "%ld\t%ld\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\n",  id,  tag,  mass,  pos.x,  pos.y,  pos.z,  vel.x,  vel.y,  vel.z,  dens,  0.0,  pres);
			fprintf(fp, "%ld\t%ld\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\n",  id,  tag,  mass,  pos.x,  pos.y,  pos.z,  acc.x,  acc.y,  acc.z,  dens,  0.0,  pres);
			#endif
		}
		void readAscii(FILE* fp){
			#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
			fscanf (fp, "%ld\t%ld\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\n", &id, &tag, &mass, &pos.x, &pos.y, NULL, &vel.x, &vel.y, NULL, &dens, NULL, &pres);
			#else
			fscanf (fp, "%ld\t%ld\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\t%16.16lf\n", &id, &tag, &mass, &pos.x, &pos.y, &pos.z, &vel.x, &vel.y, &vel.z, &dens, NULL, &pres);
			#endif
		}
		void initialize(){
			smth = PARAM::SMTH * pow(mass / dens, 1.0/(PS::F64)(PS::DIMENSION));
		}
	};

	namespace EPI{
		class Hydro{
			public:
			PS::F64vec pos;
			PS::F64vec vel;
			PS::F64    smth;
			PS::F64    dens;
			PS::F64    pres;
			PS::F64    snds;
			PS::F64    visc;//kinetic viscosity
			PS::S64    id;///DEBUG
			void copyFromFP(const RealPtcl& rp){
				this->pos  = rp.pos;
				this->vel  = rp.vel;
				this->smth = rp.smth;
				this->dens = rp.dens;
				this->pres = rp.pres;
				this->snds = rp.snds;
				this->id   = rp.id;///DEBUG
				this->visc = rp.visc->KineticViscosity();
			}
			PS::F64vec getPos() const{
				return this->pos;
			}
			PS::F64 getRSearch() const{
				return kernel_t::supportRadius() * this->smth;
			}
		};
	}

	namespace EPJ{
		class Hydro{
			public:
			PS::F64vec pos;
			PS::F64vec vel;
			PS::F64    dens;
			PS::F64    mass;
			PS::F64    smth;
			PS::F64    pres;
			PS::F64    snds;
			PS::F64    visc;//kinetic viscosity
			PS::S64    id;///DEBUG
			TYPE type;
			void copyFromFP(const RealPtcl& rp){
				this->pos  = rp.pos;
				this->vel  = rp.vel;
				this->dens = rp.dens;
				this->pres = rp.pres;
				this->smth = rp.smth;
				this->mass = rp.mass;
				this->snds = rp.snds;
				this->type = rp.type;
				this->id   = rp.id;
				this->visc = rp.visc->KineticViscosity();
			}
			PS::F64vec getPos() const{
				return this->pos;
			}
			PS::F64 getRSearch() const{
				return kernel_t::supportRadius() * this->smth;
			}
			void setPos(const PS::F64vec& pos){
				this->pos = pos;
			}
		};
	}
}


template <class Ptcl> class Problem{
	Problem(){
	}
	public:
	static void setupIC(PS::ParticleSystem<Ptcl>&, system_t&, PS::DomainInfo&){
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>&, system_t&){
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>&, system_t&){
	}
};

